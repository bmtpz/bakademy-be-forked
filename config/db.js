const { Sequelize } = require('sequelize');
/**
 * Creates a Sequelize object for connecting to database
 */
module.exports = new Sequelize('bakademy', 'postgres', 'solid123', {
    host: 'localhost',
    dialect: 'postgres'
});