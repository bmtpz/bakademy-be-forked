/**
 * User do not owns this course exception
 * @function
 * @Exception
 */
module.exports = function UserNotFoundException() {
    this.status = 403
    this.message = "User do not own this course" 
}