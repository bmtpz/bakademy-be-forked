/**
 * Error handler middleware for handling throwed errors from all app
 * @param {*} err 
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
module.exports = (err, req, res, next) => {
    let {status, message, errors} = err
    let validationErrors
    if (errors) {
        validationErrors = {}
        errors.forEach((error) => {
            validationErrors[error.param] = error.msg
        });
    }
    if(!status){
        status=500
        message='an error occured'
    }
    res.status(status).send({
        message: message,
        timestamp: Date.now(),
        path: req.originalUrl,
        validationErrors
    })
}