/**
 * Validation exception for request bodies
 * @function
 * @Exception
 * @param {Array} errors an array of validation errors 
 */
module.exports = function ValidationException(errors) {
    this.status = 422;
    this.message = 'Invalid Request';
    this.errors = errors;
}