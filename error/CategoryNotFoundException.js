/**
 * Category not found exception
 * @function
 * @Exception
 */
module.exports = function CategoryNotFoundException() {
    this.status = 404
    this.message = "Category not found" 
}