const express = require("express");
const courseController = require("../controller/Courses.controller");
const IdControl = require("../middleware/IdControl");
const JWTVerify = require("../middleware/JWTVerify");
const { commentValidate } = require("../middleware/validation/course/CommentandScoreValidation");
const router = express.Router();

router.get("/", courseController.getCourses)
router.get('/:id', IdControl, courseController.getCourseByID)
router.get('/categories/:id', IdControl, courseController.getCoursesByCategory)
router.put('/comments/:id', IdControl, commentValidate, JWTVerify.authenticateToken, courseController.putCommentandScore),
router.get('/users/:id', courseController.getUsersCourses)
router.post("/", JWTVerify.authenticateToken, courseController.createCourse)
module.exports = router