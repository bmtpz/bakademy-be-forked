const express = require("express");
const userController = require("../controller/User.controller");
const IdControl = require("../middleware/IdControl");
const JWTVerify = require("../middleware/JWTVerify");
const router = express.Router();

router.get("/", userController.getUsers)
router.get('/:id', IdControl, userController.getUserByID)
router.put('/buy/:id', IdControl, userController.buyCourse)
module.exports = router