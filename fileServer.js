require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");

const db = require("./config/db");
const ErrorHandler = require("./error/ErrorHandler");


const cloudinary = require('cloudinary').v2;
const { CloudinaryStorage } = require('multer-storage-cloudinary');
const multer = require("multer");
const app = express();

cloudinary.config({
  cloud_name: "bakademy",
  api_key: "434681124335783",
  api_secret: "2Btzllvev8Owl4mxUwGhY1mpelo",
});
const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: "DEV",
    format: async (req, file) => 'mp4',
    resource_type: (req, file) => 'video'
  },
});

const upload = multer({
  storage: storage,
});
app.use(cors());
app.options("*", cors());

db.authenticate()
  .then(() => console.log("Connection has been established successfully."))
  .catch(() => console.error("Unable to connect to the database:", error));

app.post("/api/v1/upload", upload.array("files"), (req, res) => {
  try {
    console.log({ files: req.files})
    return res.json({ files: req.files});
  } catch (error) {
    console.log(error);
  }
});
app.use(ErrorHandler);
db.sync().then(() => {
  app.listen(5000, () => console.log("server started"));
});
