const userService = require("../service/User.service");
/**
 * Exporting controller functions in an object
 * @controller
 */
let userController = {
  getUsers: getUsers,
  getUserByID: getUserByID,
  buyCourse
};

/**
 * Controller function for getting all users from db
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getUsers(req, res, next) {
  try {
    const users = await userService.getUsers()
    res.send(users)
  } catch (error) {
    next(error)
  }
}

/**
 * Controller function for getting users by id from db
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getUserByID(req, res, next) {
  try {
    const user = await userService.getUserByID(req.params.id)
    res.send(user)
  } catch (error) {
    next(error)
  }
}

/**
 * Controller function which let user to buy specific course
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function buyCourse(req, res, next) {

  try {
    const userId = req.userId
    const courseId = req.params.id
    await userService.buyCourse(userId, courseId)
    res.json('course purchased')
  } catch (err) {
    next(err)
  }
}

module.exports = userController