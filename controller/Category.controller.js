const categoryService = require("../service/Category.service");
/**
 * Exporting controller functions in an object
 * @controller
 */
let categoryController = {
    getCategories,
    getCategoryByID
}

/**
 * Controller function for getting categories from db
 * @function
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getCategories(req, res, next){
    try {
        const categories = await categoryService.getCategories()
        res.send(categories)
    } catch (error) {
        console.log(error)
        next(error)
    }
}

/**
 * Controller function for getting category from db by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getCategoryByID (req, res, next) {
    try {
        const category = await categoryService.getCategoryByID(req.params.id)
        res.send(category)
    } catch (error) {
        console.log(error)
        next(error)
    }
}

module.exports = categoryController