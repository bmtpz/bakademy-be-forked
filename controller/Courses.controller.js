const courseService = require("../service/Courses.service");

/**
 * Exporting controller functions in an object
 * @controller
 */
let courseController = {
    getCourseByID,
    getCourses,
    getCoursesByCategory,
    putCommentandScore,
    getUsersCourses,
    createCourse
}

/**
 * Controller function for getting Courses from db
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getCourses(req, res, next) {

    try {
        const courses = await courseService.getCourses()
        res.send(courses)
    } catch (error) {
        next(error)
    }
}

/**
 * Controller function for getting Course from db by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function getCourseByID(req, res, next) {
    try {
        const course = await courseService.getCourseByID(req.params.id)
        res.send(course)
    } catch (error) {
        console.log(error)
        next(error)
    }
}

/**
 * Controller function for getting Courses from db by given category id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 */
async function getCoursesByCategory(req, res) {
    const courses = await courseService.getCoursesByCategory(req.params.id)
    res.send(courses)
}
/**
 * Controller function for letting user to put comment and score to specific course which he owns
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.function} next 
 */
async function putCommentandScore(req, res, next) {
    courseId = req.params.id
    userId = req.userId
    comment = req.body.comment
    score = req.body.score
    try {
        await courseService.putCommentandScore(userId, courseId, comment, score)
        res.json("comment and score added")
    } catch (err) {
        next(err)
    }
}

async function getUsersCourses(req, res, next) {
    const userId = req.params.id
    try {
        const courses = await courseService.getUsersCourses(userId)
        res.send(courses)
    } catch (error) {
        next(err)
    }
}

async function createCourse(req, res, next) {
    const userId = req.userId
    const course = req.body.course
    const contents = req.body.contents
    try {
        await courseService.createCourse(course, contents, userId)
        res.status(200).send('Course Created')
    } catch (error) {
        next(error)
    }
}

module.exports = courseController