const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
/**
 * Represents relation between Users and Courses
 */
const Users_Courses = db.define("Users_Courses", {
    comment: {
        type: DataTypes.STRING(1000),
        allowNull: true
    },
    UserScore: {
        type: DataTypes.FLOAT,
        allowNull: true
    }
  });
  
  module.exports = Users_Courses;