const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
const Course = require("./Course.model");
/**
 * Represents category model
 */
const Content = db.define("Contents", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    path: {
        type: DataTypes.STRING,
        allowNull: false
    },
    fileName: {
        type: DataTypes.STRING,
        allowNull:false
    },
    order: {
        type: DataTypes.INTEGER,
        allowNull:false
    }
  });

  Content.belongsTo(Course)
  Course.hasMany(Content)
  
  module.exports = Content;