const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
/**
 * Represents Token model
 */
const Token = db.define("Tokens", {
    id: {
        type: DataTypes.INTEGER ,
        unique: true,
        primaryKey: true, 
    },
  
    token:{
        type: DataTypes.STRING
    }
  });
  
  module.exports = Token;