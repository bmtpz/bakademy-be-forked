const { Sequelize, DataTypes } = require("sequelize");
const db = require("../config/db");
const User = require("./User.model");
/**
 * Represents role model
 */
const Role = db.define("Roles", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  roleName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

/** Defines relation to Roles To Users */
Role.belongsToMany(User, {
  through: 'Users_Roles',
  onDelete: "cascade",
});

/** Defines relation Users to Roles */
User.belongsToMany(Role, {
  through: 'Users_Roles',
  onDelete: "cascade",
});

module.exports = Role;
