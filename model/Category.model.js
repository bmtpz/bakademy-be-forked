const {Sequelize, DataTypes} = require("sequelize");
const db = require("../config/db");
/**
 * Represents category model
 */
const Category = db.define("Categories", {
    categoryName: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
  
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  });
  
  module.exports = Category;