const User = require("../model/User.model");
const bcrypt = require('bcrypt');
const UserNotFoundException = require("../error/UserNotFoundException");
//const courseService = require("./Courses.service");
const Role = require("../model/Role.model");
const Course = require("../model/Course.model");
/**
 * Exporting functions in an object
 * @service
 */
let userService = {
  getUsers: getUsers,
  getUserByID: getUserByID,
  getUserByEmail: getUserByEmail,
  addUser: addUser,
  buyCourse
};
/**
 * Getting all users from database
 * @function
 * @returns ALL USERS
 */
async function getUsers() {
  return await User.findAll()
}

/**
 * Get specific user by id
 * @function
 * @param {number} id 
 * @returns USER
 */
async function getUserByID(id) {
  const user = await User.findByPk(id)
  if (!user) {
    throw new UserNotFoundException()
  }
  return user
}

/**
 * Add user to database by given object
 * @function
 * @param {object} user 
 * @returns CREATED USER
 */
async function addUser(user) {
  const salt = await bcrypt.genSalt()
  const hashedPassword = await bcrypt.hash(user.password, salt)
  user.password = hashedPassword
  const createdUser = await User.create(user)
  const userRole = await Role.findByPk(3)
  return await createdUser.addRole(userRole)
}

/**
 * Get specific user from database by email
 * @function
 * @param {string} email 
 * @returns USER
 */
async function getUserByEmail(email) {
  const user = await User.findOne({ where: { email: email }, raw: true });
  return user
}

/**
 * Lets a user to own a course by given user and course id
 * @function
 * @param {number} userId 
 * @param {number} courseId 
 */
async function buyCourse(userId, courseId) {
  try {
    const user = await getUserByID(userId)
    const course = await Course.findByPk(courseId)
    await user.addCourse(course)
  } catch (error) {
    throw(error)
  }
}
module.exports = userService