const Category = require("../model/Category.model");
const CategoryNotFoundException = require('../error/CategoryNotFoundException')
/**
 * Exporting functions in an object
 * @service
 */
let categoryService = {
    getCategories,
    getCategoryByID
}

/**
 * Get all Categories from database
 * @returns CATEGORIES
 */
async function getCategories () {
    return await Category.findAll()
}

/**
 * Get specific category by id
 * @param {number} id 
 * @returns CATEGORY
 */
async function getCategoryByID (id) {
    const category = await Category.findByPk(id)
    if (!category) {
        throw new CategoryNotFoundException()
    }
    return category
}

module.exports = categoryService