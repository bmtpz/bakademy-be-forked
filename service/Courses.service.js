const Category = require("../model/Category.model");
const Course = require("../model/Course.model");
const { Op } = require("sequelize");
const Users_Courses = require("../model/Users_Courses.model");
const CourseNotFoundException = require("../error/CourseNotFoundException");
const UserDoNotOwnsCoursException = require("../error/UserDoNotOwnsCourseException");
const User = require("../model/User.model");
const Content = require("../model/Content.model");
const userService = require("./User.service");
/**
 * Exporting functions in an object
 * @service
 */
let courseService = {
  getCourses,
  getCourseByID,
  getCoursesByCategory,
  putCommentandScore,
  getUsersCourses,
  createCourse,
};

/**
 * Get all Courses from database
 * @function
 * @returns ALL COURSES
 */
async function getCourses() {
  return await Course.findAll({
    attributes: {
      exclude: ["description", "updatedAt", "instructorId", "score", "UserId"],
    },
    include: {
      model: User,
      as: "instructor",
      attributes: ["firstName", "lastName", "fullName"],
    },
  });
}

/**
 * Get specific course by id
 * @param {number} id
 * @returns COURSE
 */
async function getCourseByID(id) {
  const course = await Course.findByPk(id, {
    attributes: { exclude: ["updatedAt", "instructorId", "score", "UserId"] },
    include: [{
      model: User,
      as: "instructor",
      attributes: ["firstName", "lastName", "fullName"],
    },
    {
      model:Content,
    }  
  ]
  });
  if (!course) {
    throw new CourseNotFoundException();
  }
  return course;
}

/**
 * Get courses which belongs to specfic category by given id
 * @param {number} id
 * @returns COURSES
 */
async function getCoursesByCategory(id) {
  const courses = await Course.findAll({
    include: [
      {
        model: Category,
        where: { [Op.or]: [{ id: id }, { parent_id: id }] },
      },
    ],
  });

  return courses;
}

/**
 * Lets user to add comment and score to course
 * @param {number} userId
 * @param {number} courseId
 * @param {string} comment
 * @param {number} score
 * @returns COURSE
 */
async function putCommentandScore(userId, courseId, comment, score) {
  const course = await Users_Courses.findOne({
    where: { [Op.and]: [{ UserId: userId }, { CourseId: courseId }] },
  });
  if (!course) {
    throw new UserDoNotOwnsCoursException();
  }
  course.update({ comment: comment });
  course.update({ UserScore: score });
  await Course.increment("scoreCounter", { by: 1, where: { id: courseId } });
  await Course.increment("score", { by: score, where: { id: courseId } });
  return course;
}

async function getUsersCourses(id) {
  const courses = Course.findAll({
    include: [{ model: User, where: { id: id } }],
  });
  return courses;
}

async function createCourse(course, contents, insId) {
  const ins = await userService.getUserByID(insId);
  const cr_course = await Course.create(course);
  await cr_course.setInstructor(ins);
  contents.forEach(async (element, index) => {
    let content = await Content.create({
      path: element.path,
      fileName: element.filename,
      order: (index+1),
    });
    await content.setCourse(cr_course);
  });
  return cr_course;
}
module.exports = courseService;
