const { body, validationResult } = require('express-validator');
const ValidationException = require('../../../error/ValidationException');
/**
 * an Array of rules to validate body
 */
exports.commentValidate = [
   body('comment')
   .isString().withMessage('Please only enter string')
   .bail()
   .isLength({max:1000}).withMessage('length must be max 1000 characters'),
   body('score')
   .notEmpty().withMessage('score cannot be null')
   .isNumeric().withMessage('Score must be numeric'),
   /**
    * Middleware function for throw validation errors or move on with next layer
    * @param {Express.Request} req 
    * @param {Express.Response} res 
    * @param {Express.Response} next 
    */
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            return next(new ValidationException(errors.array()));
        next();
    },
];